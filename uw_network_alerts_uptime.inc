<?php

/**
 * @file
 * Functions for Network and Service Alerts.
 */

/**
 * Page callback for page showing list of alerts.
 *
 * @return array
 *   The render array.
 */
function uw_network_alerts_uptime($form, &$form_state) {

  // Add the JS from D3.
  $library_path_d3 = libraries_get_path('d3');
  drupal_add_js($library_path_d3 . '/d3.min.js', 'file');

  // Add our JS for the network alerts.
  drupal_add_js(drupal_get_path('module', 'uw_network_alerts') .'/js/uw_network_alerts_uptime.js', 'file');

  // Add the CSS for the network alerts.
  drupal_add_css(drupal_get_path('module', 'uw_network_alerts') .'/css/uw_network_alerts_timeline.css');

  // The page array.
  $page = [];

  // Variable to store and use for the number of days in the timeline.
  $days_for_timeline = 90;

  // From and to dates to be used.
  $todate = date('Y-m-d');
  $fromdate = date('Y-m-d', strtotime($todate . "-" . $days_for_timeline . " days"));
  $fromdate_test = date('m/j/Y', strtotime($todate . "-" . $days_for_timeline . " days"));

  // Add the from and to date to the JS.
  drupal_add_js(array('todate' => $todate), 'setting');
  drupal_add_js(array('fromdate' => $fromdate), 'setting');

  // To and from DateTimeInterfaces.
  $todate_dti = new DateTime($todate);
  $fromdate_dti = new DateTime($fromdate);

  // The interval between the to and from dates.
  $to_from_interval = date_diff($todate_dti, $fromdate_dti);

  // Get the seconds between the to and from dates.
  // Will be used in the uptime calculation.
  $to_from_seconds = (($to_from_interval->days * 24 + $to_from_interval->h) * 60 + $to_from_interval->i) * 60 + $to_from_interval->s;

  // Get the all the services.
  $services = _uw_network_alerts_get_services();

  // Step through each service and put in the all dates from the days_for_timeline.
  foreach ($services as $service) {

    // If there is a service, continue to process.
    if ($service->v !== '-' && $service->v !== '') {

      // Get the uptimes using the service and the from and to dates.
      $uptimes = uw_network_alerts_alert_list($service->v, '', $fromdate, $todate);

      // Fill the array with all dates for 90 days.
      for ($i = 0; $i < $days_for_timeline; $i++) {

        // Variable to track total seconds.
        $total_seconds = 0;

        // Variable to store all the dates for the timeline.
        $timeline[$service->v][] = array(
          'time_date' => date("m/j/Y", strtotime($fromdate . "+" . $i . " days")),
          'time_date_display' => date("M j, Y", strtotime($fromdate . "+" . $i . " days")),
          'type' => 'Up',
        );
      }

      // Get the alerts that are downed or maintenace.
      $timeline[$service->v] = _uw_network_alerts_get_timeline($timeline[$service->v], $service->v, $fromdate, $todate, $fromdate_test, $total_seconds);

      // Variable to store all the info about the timeline.
      $timeline_info[$service->v]['service_name'] = uw_network_alerts_get_service_name($service);
      $timeline_info[$service->v]['total_seconds'] = $total_seconds;
      $timeline_info[$service->v]['service'] = $service->v;
    }
  }

  // Add days for timeline variable to be used in JS.
  drupal_add_js(array('days_for_timeline' => $days_for_timeline), 'setting');

  // The variable to add to Drupal.settings JS.
  $timeline_data = array('timeline_data' => $timeline);

  // Add the JS with the variables.
  drupal_add_js($timeline_data, 'setting');

  // Page wrapper.
  $page['uptimes'] = [
    '#type' => 'container',
    '#attributes' => ['class' => ['uw-network-alerts-uptimes']],
  ];

  // Help links.
  $items = [
    l(t('Return to network and service alerts'), '/information-systems-technology/network-service-alerts', ['external' => TRUE]),
  ];

  $page['uptimes']['contacts'] = [
    '#theme' => 'item_list',
    '#items' => $items,
    '#attributes' => ['class' => ['uw-network-alerts-services__contact-one-item services-tab-item']],
  ];

  $service_counter = 0;
  $total_services_count = count($timeline_info);

  // Step through each of the timeline infos and setup timeline.
  foreach ($timeline_info as $key => $value) {

    // Class name that the svg is going to be appended to.
    $class_name = 'uw-network-alerts-uptimes-' . str_replace(' ', '-', $key);

    if ($service_counter >= 5 && $service_counter % 5 == 0 && ($total_services_count - $service_counter) > 5) {
      $uptime_wrapper_class_name = 'uw-network-alerts-uptime-with-link';
    }
    else {
      $uptime_wrapper_class_name = 'uw-network-alerts-uptime';
    }

    // Wrapper for the entire uptime.
    $page['uptimes'][$key]['uptime-wrapper'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array($uptime_wrapper_class_name)),
      '#prefix' => '<a href="network-service-alerts/' . str_replace(' ', '-', $timeline_info[$key]['service']) . '">',
      '#suffix' => '</a>',
    ];

    // Wrapper for the title.
    $page['uptimes'][$key]['uptime-wrapper']['title-wrapper'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-timeline-title')),
    ];

    // The actual title.
    $page['uptimes'][$key]['uptime-wrapper']['title-wrapper']['title'] = [
      '#type' => 'markup',
      '#markup' => '<h2>' . $timeline_info[$key]['service_name'] . '</h2>',
    ];

    // Wrapper for the timeline.
    $page['uptimes'][$key]['uptime-wrapper']['timeline'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-timeline-wrapper')),
    ];

    // Variable to store the alert count.
    $alert_count = 0;

    // Step through each current moment alert and get count and status.
    foreach (uw_network_alerts_alert_list($timeline_info[$key]['service'], '', date('Y-m-d g:i:s'), date('Y-m-d g:i:s')) as $alert_row) {
      $alert_count++;
      $alert_type = ucfirst(strtolower($alert_row->incidenttype));
    }

    // If there are no current alerts, the service is operational.
    if ($alert_count == 0) {
      $alert_type = 'Operational';
    }

    // Wrapper for the operational status.
    $page['uptimes'][$key]['uptime-wrapper']['timeline']['operational_status_wrapper'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-operational-status')),
    ];

    // The operational status.
    $page['uptimes'][$key]['uptime-wrapper']['timeline']['operational_status_wrapper']['operational_status'] = [
      '#type' => 'markup',
      '#markup' => 'Current status: <span class="' . $alert_type . '">' . $alert_type . '</span>',
    ];

    // Wrapper for the timeline, this is where the svg will be appended to.
    $page['uptimes'][$key]['uptime-wrapper']['timeline']['timeline'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-timeline', $class_name), 'aria-hidden' => 'true'),
    ];

    // Wrapper for the uptime calculation.
    $page['uptimes'][$key]['uptime-wrapper']['timeline']['uptime'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-uptime-calculation')),
    ];

    // Uptime calculations.
    $uptime = ($to_from_seconds - $value['total_seconds']) / $to_from_seconds * 100;
    $uptime = round($uptime, 2);
    
    // The actual uptime text.
    $page['uptimes'][$key]['uptime-wrapper']['timeline']['uptime']['uptime_display'] = [
      '#type' => 'markup',
      '#markup' => $uptime . '% uptime for ' . $days_for_timeline . ' days',
    ];

    // Store the class name so we know where to append the svg to.
    $timeline_info[$key]['class_name'] = $class_name;

    if ($service_counter >= 5 && $service_counter % 5 == 0 && ($total_services_count - $service_counter) > 5) {
      $page['uptimes']['contacts' . $service_counter] = [
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => ['class' => ['uw-network-alerts-services__contact-one-item services-tab-item']],
      ];
    }

    $service_counter++;
  }

  $page['uptimes']['contacts-last'] = [
    '#theme' => 'item_list',
    '#items' => $items,
    '#attributes' => ['class' => ['uw-network-alerts-services__contact-one-item services-tab-item']],
  ];

  // Add the JS with the variables.
  drupal_add_js(array('timeline_info' => $timeline_info), 'setting');
  
  return $page;
}
