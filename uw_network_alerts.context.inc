<?php

/**
 * @file
 * uw_network_alerts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_network_alerts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_ist_downed_services_homepage';
  $context->description = 'IST downed services homepage blog';
  $context->tag = 'homepage';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_network_alerts-frontpage_service_alerts_block' => array(
          'module' => 'uw_network_alerts',
          'delta' => 'frontpage_service_alerts_block',
          'region' => 'promo',
          'weight' => '-48',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('IST downed services homepage blog');
  t('homepage');
  $export['uw_ist_downed_services_homepage'] = $context;

  return $export;
}
