<?php

/**
 * @file
 * Functions for Network and Service Alerts.
 */

/**
 * Page callback for page showing list of alerts.
 *
 * @param string $service
 *   The name of the servce. Use NULL for all.
 *
 * @return array
 *   The render array.
 */
function uw_network_alerts_page($form, &$form_state, $service = NULL) {

  // Get the parameters from the URL.
  $query_parameters = drupal_get_query_parameters();

  // If there is no service set and there is a service filter, set the service.
  if (!$service && isset($query_parameters['service_filter'])) {
    $service = $query_parameters['service_filter'];
  }
  else {
    $query_parameters['service_filter'] = $service;
  }

  // If there is a service, set title, breadcrumbs and timeline.
  if ($service) {

    // Variable to hold the number of days for the timeline.
    $days_for_timeline = 90;

    // Set the from and to date based on the filters.
    if (isset($query_parameters['from_date_filter']) && isset($query_parameters['to_date_filter'])) {
      $todate = $query_parameters['to_date_filter'];
      $fromdate = $query_parameters['from_date_filter'];
      $fromdate_test = date('m/j/Y', strtotime($query_parameters['from_date_filter']));
    }
    else if (isset($query_parameters['from_date_filter']) && !isset($query_parameters['to_date_filter'])) {
      $fromdate = $query_parameters['from_date_filter'];
      $fromdate_test = date('m/j/Y', strtotime($query_parameters['from_date_filter']));
      $todate = date('Y-m-d');
    }
    else if (!isset($query_parameters['from_date_filter']) && isset($query_parameters['to_date_filter'])) {
      $todate = $query_parameters['to_date_filter'];
      $fromdate = date('Y-m-d', strtotime($todate . " -" . $days_for_timeline . " days"));
      $fromdate_test = date('m/j/Y', strtotime($fromdate));
    }
    else {
      $todate = date('Y-m-d');
      $fromdate = date('Y-m-d', strtotime($todate . "-" . $days_for_timeline . " days"));
      $fromdate_test = date('m/j/Y', strtotime($todate . "-" . $days_for_timeline . " days"));
    }

    // To and from DateTimeInterfaces.
    $todate_dti = new DateTime($todate);
    $fromdate_dti = new DateTime($fromdate);

    // The interval between the to and from dates.
    $to_from_interval = date_diff($todate_dti, $fromdate_dti);

    // Variable to hold the number of days for the timeline.
    $days_for_timeline = $to_from_interval->days;

    if ($days_for_timeline <= 90) {

      // Get the seconds between the to and from dates.
      // Will be used in the uptime calculation.
      $to_from_seconds = (($to_from_interval->days * 24 + $to_from_interval->h) * 60 + $to_from_interval->i) * 60 + $to_from_interval->s;

      // Total seconds of downtime, used for uptime calculation.
      $total_seconds = 0;

      // Add the JS from D3.
      $library_path_d3 = libraries_get_path('d3');
      drupal_add_js($library_path_d3 . '/d3.min.js', 'file');

      // Add our JS for the network alerts.
      drupal_add_js(drupal_get_path('module', 'uw_network_alerts') .'/js/uw_network_alerts_timeline.js', 'file');

      // Add the CSS for the network alerts.
      drupal_add_css(drupal_get_path('module', 'uw_network_alerts') .'/css/uw_network_alerts_timeline.css');

      // Add the from and to dates to the JS.
      drupal_add_js(array('todate' => $todate), 'setting');
      drupal_add_js(array('fromdate' => $fromdate), 'setting');

      // Add days for timeline variable to be used in JS.
      drupal_add_js(array('days_for_timeline' => $days_for_timeline), 'setting');

      // Fill the array with all dates for 90 days.
      for ($i = 0; $i < $days_for_timeline; $i++) {
        $timeline[] = array(
          'time_date' => date("m/j/Y", strtotime($fromdate . "+" . $i . " days")),
          'time_date_display' => date("M j, Y", strtotime($fromdate . "+" . $i . " days")),
          'type' => 'Up',
        );
      }

      // The number of alerts at the current moment.
      $alert_count = 0;

      // Step through each current moment alert and get count and status.
      foreach (uw_network_alerts_alert_list($service, '', date('Y-m-d g:i:s'), date('Y-m-d g:i:s')) as $alert_row) {
        $alert_count++;
        $alert_type = ucfirst(strtolower($alert_row->incidenttype));
      }

      $timeline = _uw_network_alerts_get_timeline($timeline, $service, $fromdate, $todate, $fromdate_test, $total_seconds);

      // Step through each of the timelines and separate out the ups, incidents and mainteance.
      foreach($timeline as $tl) {

        foreach ($tl as $key => $t) {

          if ($key == "up") {
            $ups[] = $t;
          }

          if ($key == "Incident") {
            $incidents[] = $t;
          }

          if ($key == "Maintenance") {
            $maintenances[] = $t;
          }
        }
      }

      // The variable to add to Drupal.settings JS.
      $timeline_data = array('timeline_data' => $timeline);

      // Add the ups to JS.
      if (isset($ups)) {
        drupal_add_js(array('ups' => $ups), 'setting');
      }

      // If there are incidents add them to JS.
      if (isset($incidents)) {
        drupal_add_js(array('incidents' => $incidents), 'setting');
      }

      // If there are mainteances add them to JS.
      if (isset($maintenances)) {
        drupal_add_js(array('maintenances' => $maintenances), 'setting');
      }

      // Add the JS with the variables.
      drupal_add_js($timeline_data, 'setting');
    }

    // Set the title.
    drupal_set_title(uw_network_alerts_get_service_name((object) ['v' => $service]));

    // Set the breadcrumbs.
    $breadcrumb = [];
    $breadcrumb[] = l(t('Home'), '<front>');
    $breadcrumb[] = l(t('Network and Service Alerts'), 'network-service-alerts');
    drupal_set_breadcrumb($breadcrumb);
  }

  // Get the type of alert from filters.
  $type = isset($query_parameters['type_filter']) ? $query_parameters['type_filter'] : NULL;

  // Get the from date from filters.
  $fromdate = isset($query_parameters['from_date_filter']) ? $query_parameters['from_date_filter'] : NULL;
  
  // Get the to date from filters.
  $todate = isset($query_parameters['to_date_filter']) ? $query_parameters['to_date_filter'] : NULL;

  // If there is an order parameter, set variable to use and unset parameter.
  // If there is no order parameter, default to start_time.
  $order_parameter = isset($query_parameters['order']) ? $query_parameters['order'] : 'starttime';
  unset($query_parameters['order']);

  // If there is an sort parameter, set variable to use and unset parameter.
  // If there is no sort parameter, default to asc.
  $sort_parameter = isset($query_parameters['sort']) ? $query_parameters['sort'] : 'desc';
  unset($query_parameters['sort']);

  // Array to store information about table sorting headers.
  $orders['start_time'] = array('name' => 'Start Time', 'order' => 'starttime', 'sort' => 'desc');
  $orders['service'] = array('name' => 'Service', 'order' => 'service', 'sort' => 'desc');
  $orders['alerttype'] = array('name' => 'Type', 'order' => 'alerttype', 'sort' => 'desc');

  // Limit number of alerts to display. Default limit of 10; allow for "all".
  $limit = isset($query_parameters['show']) ? $query_parameters['show'] : NULL;
  if ($limit === 'all') {
    $limit = NULL;
  }
  elseif ($limit && (int) $limit > 0) {
    $limit = (int) $limit;
  }
  else {
    $limit = 25;
  }

  // If there is a page parameter set page variable.
  if (isset($query_parameters['page'])) {
    $pager = $query_parameters['page'];
  }
  else {
    $pager = NULL;
  }

  // Count of items.
  $items_count = 0;

  // Step through each alert and format to a table.
  foreach (uw_network_alerts_alert_list($service, $type, $fromdate, $todate, $limit, $order_parameter, $sort_parameter, $pager) as $alert_row) {

    // If we don't have any items, get the header for theme_table.
    if ($items_count == 0) {
      $url = url('/network-service-alerts', array('query' => $query_parameters));

      // Step through each of the table orders and process them.
      foreach ($orders as $key => $value) {

        // Set the default variables for each order.
        $orders[$key]['parameters'] = $query_parameters;
        $orders[$key]['parameters']['order'] = $orders[$key]['order'];
        $orders[$key]['parameters']['sort'] = 'desc';
        $orders[$key]['th_class'] = array('sortable', $orders[$key]['parameters']['order']);
        $orders[$key]['link_class'] = 'sortable-link';

        // If the order is the sort parameter, process it.
        if ($orders[$key]['order'] == $order_parameter) {

          // If it is set to sort asc, set query parameter to desc and set th and link classes.
          // If it nor sort asc, set query parameter to asc and set th and link classes.
          if ($sort_parameter == 'asc') {
            $orders[$key]['parameters']['sort'] = 'desc';
            $orders[$key]['th_class'] = array('sortable', 'active', $orders[$key]['parameters']['order']);
            $orders[$key]['link_class'] = array('sortable-link', 'asc');
          }
          else {
            $orders[$key]['parameters']['sort'] = 'asc';
            $orders[$key]['th_class'] = array('sortable', 'active', $orders[$key]['parameters']['order']);
            $orders[$key]['link_class'] = array('sortable-link', 'desc');
          }
        }
      }

      // Step through each of the orders and set up the headers.
      foreach ($orders as $order) {
        $headers[] = array(
          'data' => l($order['name'], '/network-service-alerts', array('query' => $order['parameters'], 'attributes' => array('class' => $order['link_class']))),
          'class' => $order['th_class'],
        );
      }

      // Add the description header.
      $headers[] =  array(
        'data' => 'Description',
        'class' => array('description'),
      );
    }

    // Get the alerts.
    $alert = uw_network_alerts_get_alert_title($alert_row, TRUE);

    // Convert the date to a timestamp, so that we can format the date.
    $timestamp = strtotime($alert['start_time']);

    // Get the row data to be used in theme_table.
    $rows[] = array(
              array(
                'data' => date('M d, Y g:i a', $timestamp),
              ),
              array(
                'data' => uw_network_alerts_get_service_name(NULL, $alert['service']),
              ),
              array(
                'data' => ucwords($alert['type']),
              ),
              array(
                'data' => l($alert['description'], 'network-service-alert/' . $alert_row->sequence),
              ),
            );

    // Increment the counter.
    $items_count++;
  }

  // The page array.
  $page = [];

  // The container for the page.
  $page['alerts'] = [
    '#type' => 'container',
    '#attributes' => ['class' => ['uw-network-alerts-list']],
  ];

  if ($service == NULL || $service == '') {
    $page['alerts']['dashboard'][] = [
        'uw_network_alerts_service_dashboard' => uw_network_alerts_service_dashboard(),
    ];
  }

  // If there are items dispaly filters and alerts.
  // If there are no items display message about no items found.
  if ($items_count > 0) {

    if ($service && $days_for_timeline <= 90) {

      // Container for the timeline.
      $page['alerts']['timeline_container'] = [
        '#type' => 'container',
        '#attributes' => array('class' => array('uw-network-alerts-timeline-wrapper')),
      ];

      if ($alert_count == 0) {
        $alert_type = 'Operational';
      }

      // Container for the operational status.
      $page['alerts']['timeline_container']['operational_status_wrapper'] = [
        '#type' => 'container',
        '#attributes' => array('class' => array('uw-network-alerts-operational-status')),
      ];

      // Container for the timeline.
      $page['alerts']['timeline_container']['operational_status_wrapper']['operational_status'] = [
        '#type' => 'markup',
        '#markup' => 'Current status: <span class="' . $alert_type . '">' . $alert_type . '</span>',
      ];

      // The actual timeline.
      $page['alerts']['timeline_container']['timeline'] = [
        '#type' => 'container',
        '#attributes' => array('class' => ['uw-network-alerts-timeline'], 'id' => 'uw-network-alerts-timeline', 'aria-hidden' => 'true'),
      ];

      // The container for the uptime.
      $page['alerts']['timeline_container']['uptime_wrapper'] = [
        '#type' => 'container',
        '#attributes' => array('class' => array('uw-network-alerts-uptime-calculation')),
      ];

      // Uptime calculations.
      $uptime = ($to_from_seconds - $total_seconds) / $to_from_seconds * 100;
      $uptime = round($uptime, 2);

      // The actual uptime text.
      $page['alerts']['timeline_container']['uptime_wrapper']['uptime'] = [
        '#type' => 'markup',
        '#markup' => $uptime . '% uptime for ' . $days_for_timeline . ' days',
      ];
    }
    elseif (isset($days_for_timeline) && $days_for_timeline > 90) {
      // Container for the timeline.
      $page['alerts']['timeline_container'] = [
        '#type' => 'container',
        '#attributes' => array('class' => array('uw-network-alerts-timeline-wrapper')),
      ];

      $page['alerts']['timeline_container']['timeline-wrapper'] = [
        '#type' => 'container',
        '#attributes' => array('class' => array('uw-network-alerts-timeline-no-display')),
      ];

      $page['alerts']['timeline_container']['timeline-wrapper']['timeline'] = [
        '#type' => 'markup',
        '#markup' => 'The timeline feature will not display for date ranges greater than 90 days. To see a visual representation of service uptime, please select a range of 90 days or less.',
      ];
    }

    // The field form for the filters.
    $page['alerts']['form'] = [
      '#type' => 'form',
    ];

    // The fieldset for the filters.
    $page['alerts']['form']['filters'] = [
      '#type' => 'fieldset',
      '#title' => 'Filters',
      '#attributes' => array('class' => array('uw-network-alerts-filters')),
    ];

    // The fieldset for date ranges filter.
    $page['alerts']['form']['filters']['date_range'] = [
      '#type' => 'fieldset',
      '#title' => 'Date range',
      '#attributes' => array('class' => array('uw-network-alerts-filters__date-range')),
    ];

    // The To date for the date ranges filter.
    $page['alerts']['form']['filters']['date_range']['start'] = [
      '#label' => 'From',
      '#type' => 'date_popup',
      '#title' => 'From',
      '#date_type' => DATE_DATETIME,
      '#date_label_position' => 'none',
      '#date_timezone' => date_default_timezone(),
      '#date_format' => 'Y-m-d',
      '#date_increment' => 1,
      '#date_year_range' => '-3:+3',
      '#default_value' => isset($query_parameters['from_date_filter']) ? $query_parameters['from_date_filter'] : '',
      '#id' => 'from_date',
    ];

    // The From date for the date ranges filter.
    $page['alerts']['form']['filters']['date_range']['end'] = [
      '#label' => 'To',
      '#type' => 'date_popup',
      '#title' => 'To',
      '#date_type' => DATE_DATETIME,
      '#date_label_position' => 'none',
      '#date_timezone' => date_default_timezone(),
      '#date_format' => 'Y-m-d',
      '#date_increment' => 1,
      '#date_year_range' => '-3:+3',
      '#default_value' => isset($query_parameters['to_date_filter']) ? $query_parameters['to_date_filter'] : '',
      '#id' => 'to_date',
    ];

    // Get the options to be used for services.
    $service_options[] = '';
    foreach (_uw_network_alerts_get_services() as $service_option) {
      if ($service_option->v !== '' && $service_option->v !== '-') {
        $service_options[$service_option->v] = uw_network_alerts_get_service_name($service_option);
      }
    }

    // Sort the array by value for services.
    asort($service_options);

    $page['alerts']['form']['filters']['filter_area'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-filters__filter-area')),
    ];

    // The services filter.
    $page['alerts']['form']['filters']['filter_area']['service'] = [
      '#type' => 'select',
      '#title' => 'Service',
      '#options' => $service_options,
      '#default_value' => isset($query_parameters['service_filter']) ? $query_parameters['service_filter'] : '',
    ];

    // Get the options to be used for types of alerts.
    $type_options[] = '';
    foreach (_uw_network_alerts_get_types() as $type_option) {
      if ($type_option->area !== '' && $type_option->area !== '-') {
        $type_options[$type_option->area] = uw_network_alerts_get_type_name($type_option->area);
      }
    }

    // The type of alert filter.
    $page['alerts']['form']['filters']['filter_area']['type'] = [
      '#type' => 'select',
      '#title' => 'Type',
      '#options' => $type_options,
      '#default_value' => isset($query_parameters['type_filter']) ? $query_parameters['type_filter'] : '',
    ];

    // The submit button for the filters.
    $page['alerts']['form']['filters']['filter_area']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Apply'),
    );

    // The submit button for the filters.
    $page['alerts']['form']['filters']['filter_area']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );

    // The submit function to be used for the filters.
    $page['#submit'][] = '_uw_network_alerts_filter_submit';

    // Use theme_table to generate html for the alerts table.
    $html = theme_table(
      array(
        'header' => $headers,
        'rows' => $rows,
        'attributes' => array(),
        'caption' => '',
        'empty' => '',
        'colgroups' => array(),
        'sticky' => TRUE,
      )
    );

    // The actual alerts.
    $page['alerts']['alerts'] = [
      '#prefix' => '<a id="network-service-alerts"><h2>Alerts</h2></a>',
      '#type' => 'markup',
      '#markup' => $html,
    ];

    // Get the total number of alerts.
    foreach(uw_network_alerts_alert_list($service, $type, $fromdate, $todate, $limit, NULL, NULL, NULL, TRUE) as $count) {
      $alert_count = $count->type_count;
    }

    // Get the total number of pages.
    $page_count = intval($alert_count / $limit);

    // Initialize the pager.
    $current_page = pager_default_initialize($alert_count, $limit);

    // The pager.
    $page['alerts']['pager'] = [
      '#theme' => 'pager',
      '#quantity' => 5,
      '#parameters' => $query_parameters,
    ];
  }
  else {
    // Container for the timeline.
    $page['alerts']['timeline_container'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-timeline-wrapper')),
    ];

    if ($alert_count == 0) {
      $alert_type = 'Operational';
    }

    // Container for the operational status.
    $page['alerts']['timeline_container']['operational_status_wrapper'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-operational-status')),
    ];

    // Container for the timeline.
    $page['alerts']['timeline_container']['operational_status_wrapper']['operational_status'] = [
      '#type' => 'markup',
      '#markup' => 'Current status: <span class="' . $alert_type . '">' . $alert_type . '</span>',
    ];

    // The actual timeline.
    $page['alerts']['timeline_container']['timeline'] = [
      '#type' => 'container',
      '#attributes' => array('class' => ['uw-network-alerts-timeline'], 'id' => 'uw-network-alerts-timeline', 'aria-hidden' => 'true'),
    ];

    // The container for the uptime.
    $page['alerts']['timeline_container']['uptime_wrapper'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('uw-network-alerts-uptime-calculation')),
    ];
  }
  return $page;
}

/**
 * Page callback for page showing individual alert.
 *
 * @return array
 *   The render array.
 */
function uw_network_alerts_alert() {

  // The arguments from the URL.
  $args = arg();

  // If the last arguments is not numeric, return 404.
  if (!is_numeric(end($args))) {
    return drupal_not_found();
  }
  else {

    // Get the alert id, called sequence, from arguments.
    $sequence = end($args);

    // Get the alert.
    $alert = uw_network_alerts_get_alert($sequence);

    // If there is no alert or the cound is 0, 404.
    if (!isset($alert)) {
      return drupal_not_found();
    }
    else if (is_array($alert) && count($alert) == 0) {
      return drupal_not_found();
    }
  }

  // The page array used to build the page.
  $page = [];

  // The wrapper for the alert.
  $page['uw_alert_wrapper'] = [
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-alert-wrapper')),
  ];

  // The title of the alert.
  $page['uw_alert_wrapper']['title'] = [
    '#type' => 'markup',
    '#markup' => '<h1>' . $alert->description . '</h1>',
  ];

  // Query list of all services.
  $query = "SELECT * FROM alertservice WHERE (description <> '') AND v = '" . $alert->service . "' ORDER BY description";
  $services = Database::getConnection('default', 'ona')->query($query);

  // Get the service.
  foreach($services as $s) {
    $service = $s;
  }

  // If there is a service, display it.
  if (isset($service) && $service !== '') {

    // The service being affected.
    $page['uw_alert_wrapper']['service'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Service</h3>' . uw_network_alerts_get_service_name($service),
    ];
  }

  // If there is a type of service, display it.
  if (isset($alert->incidenttype) && $alert->incidenttype !== '') {

    // The type of service.
    $page['uw_alert_wrapper']['type'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Type</h3>' . ucfirst(strtolower($alert->incidenttype)),
    ];
  }

  // If there is a comment, display it.
  if (isset($alert->comment) && $alert->comment !== '') {

    // The comment from the alert.
    $page['uw_alert_wrapper']['description'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Description</h3>' .  nl2br($alert->comment),
    ];
  }

  // If there is an end time, display it.
  if (isset($alert->starttime) && $alert->starttime !== '') {
    
    // The start time.
    $page['uw_alert_wrapper']['start_time'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Start time</h3>' . date('l, F j, Y g:i A', strtotime($alert->starttime)),
    ];
  }

  // If there is an end time, display it.
  if (isset($alert->endtime) && $alert->endtime !== '') {

    // The end time.
    $page['uw_alert_wrapper']['end_time'] = [
      '#type' => 'markup',
      '#markup' => '<h3>End time</h3>' . date('l, F j, Y g:i A', strtotime($alert->endtime)),
    ];
  }

  // If there is an impact on the alert, display it.
  if (isset($alert->impact) && $alert->impact !== '') {

    // The impact from the alert.
    $page['uw_alert_wrapper']['impact'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Impact</h3>' . $alert->impact,
    ];
  }

  // If there is a resolution on the alert, display it.
  if (isset($alert->resolution) && $alert->resolution !== '') {

    // The resolution from the alert.
    $page['uw_alert_wrapper']['resolution'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Resolution</h3>' . $alert->resolution,
    ];
  }

  // If there is a noticed submiited date on the alert, display it.
  if (isset($alert->submitted) && $alert->submitted !== '') {

    // The comment from the alert.
    $page['uw_alert_wrapper']['notice_submitted'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Notice submitted</h3>' . date('l, F j, Y g:i A', strtotime($alert->submitted)),
    ];
  }

  return $page;
}

/**
 *  Return render array for the frontpage block
 *
 * @return array
 *  The render array.
 */

function uw_network_alerts_frontpage_block() {
  $block=[];
  // Query list of all services.
  $query = "SELECT * FROM alertservice WHERE (description <> '') ORDER BY description";
  $services = Database::getConnection('default', 'ona')->query($query);
  $items =[];

  $downed_count = 0;
  $maint_count = 0;
  $downed_items = '<div class="downed-items"><div class="downed-items__title"><h3>Unavailable</h3></div><ul class="downed-items__list">';
  $maint_items = '<div class="maint-items"><div class="maint-items__title"><h3>Under maintenance</h3></div><ul class="maint-items__list">';
  while ($service = $services->fetch()) {

    $status = uw_network_alerts_get_service_status($service->v);

    // Only concerned with the services that are down or under active maintenence.
    if ($status == 'down' && $downed_count < 3) {
    // To see content on page, uncomment the line below and comment out the line above.
    //if ($downed_count < 3) {
      $downed_items .= '<li><div class="network-alert-link-wrapper"><div class="network-alert-link-icon">&#x2716;</div><div class="network-alert-link">' . l(check_plain($service->description), 'network-service-alerts/' . $service->v) . '</div></div></li>';
      $downed_count++;
    }
    else if ($status == "maint" && $maint_count < 3) {
    // To see content on page, uncomment the line below and comment out the line above.
    //else if ($downed_count >= 3 && $maint_count < 3) {
      $maint_items .= '<li><div class="network-alert-link-wrapper"><div class="network-alert-link-icon">-</div><div class="network-alert-link">' . l(check_plain($service->description), 'network-service-alerts/' . $service->v) . '</div></div></li>';
      $maint_count++;
    }
  }

  $downed_items .= '</ul></div>';
  $maint_items .= '</ul></div>';

  $markup = '';

  if ($downed_count > 0 && $maint_count > 0) {
    $markup = $downed_items . $maint_items;
  }
  else if ($downed_count > 0) {
    $markup = $downed_items;
  }
  else if ($maint_count > 0) {
    $markup = $maint_items;
  }
  else {
    $markup = '';
  }

  $block['content'] = [
    '#type' => 'markup',
    '#markup' => $markup,
  ];
  
  $block['subject'] = '<a href="network-service-alerts"><div class="network-alerts-title-wrapper"><div class="network-alerts-title">Status of services</div></div></a>';
  $block['content']['#prefix'] = '<div class="network-alerts-wrapper">';
  // Display message to the user if there are more than three services that are down.
  if ($downed_count >= 3 || $maint_count >= 3) {
    $block['content']['#suffix'] = '<p>More than three services are currently unavailable.</p><p><a href="network-service-alerts" class="status-all-services">Status of all services</a></p></div>';
  }
  else if ($downed_count == 0 && $maint_count == 0) {
    $block['subject'] = '<a href="network-service-alerts"><div class="network-alerts-title-wrapper all-services"><div class="network-alerts-title">Status of services</div><div class="all-services-up"></div></div></a>';
    $block['content']['#suffix'] = '</div>';
  }
  else {
    $block['content']['#suffix'] = '</div>';
  }

  return $block;
}

/**
 * Return render array for the service dashboard.
 *
 * @return array
 *   The render array.
 */
function uw_network_alerts_service_dashboard() {
  $page = [];

  // Help links.
  $items = [
    l(t('Report a problem'), '/information-systems-technology/services/request-tracker-rt-system', ['external' => TRUE]),
    l(t('IST Service Desk'), '/information-systems-technology/services/ist-service-desks', ['external' => TRUE]),
    l(t('See all service uptimes'), '/information-systems-technology/service-uptimes', ['external' => TRUE]),
  ];

  $page['services'] = [
    '#type' => 'container',
    '#attributes' => ['class' => ['uw-network-alerts-services']],
  ];
  
  $page['services']['contacts'] = [
    '#theme' => 'item_list',
    '#items' => $items,
    '#attributes' => ['class' => ['uw-network-alerts-services__contact services-tab-item']],
  ];

  $query_parameters = drupal_get_query_parameters();
  $days = isset($query_parameters['days']) ? $query_parameters['days'] : NULL;

  $status_messages = [
    'up' => NULL,
    'down' => 'Down',
    'degraded' => 'Degraded',
    'maint' => 'Maintenance in progress',
    'nonsvcmaint' => 'Non-service-affecting maintenance in progress',
    'futuremaint' => 'Future Maintenance',
  ];

  // Query list of all services.
  $query = "SELECT * FROM alertservice WHERE (description <> '') ORDER BY description";
  $services = Database::getConnection('default', 'ona')->query($query);
  $items = [];
  while ($service = $services->fetch()) {
    $status = uw_network_alerts_get_service_status($service->v);

    // Link service name to service alert listing.
    $item = '<div class="network-alert-link">' . l(uw_network_alerts_get_service_name($service), 'network-service-alerts/' . $service->v);

    // Display status message for any service not up.
    if (isset($status_messages[$status])) {
      $item .= ' <em>(' . $status_messages[$status] . ")</em>\n";
    }

    // Display uptime over the time period in $days.
    if ($days) {
      $uptime = uw_network_alerts_get_service_uptime($service->v, time() - ($days * 24 * 3600), time());
      $item .= '; <span class="uptime">Uptime: ' . $uptime . '</span>';
    }

    $item .= '</div>';

    // Create list item with class of the status.
    $items[] = ['class' => [$status], 'data' => $item, 'service' => uw_network_alerts_get_service_name($service)];
  }

  // Get the services from the items array.  
  $services = array_column($items, 'service');

  // Sort the array based on the services in alpha order.  
  array_multisort($services, SORT_ASC, SORT_REGULAR, $items);

  $page['services']['dashboard'] = [
    '#prefix' => '<h2>Service Dashboard</h2>',
    '#attributes' => ['class' => ['uw-network-alerts-services__dashboard']],
    '#theme' => 'item_list',
    '#items' => $items,
  ];

  return $page;
}
