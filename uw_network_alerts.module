<?php

/**
 * @file
 * Main hook implementations and shared functions.
 */

include_once 'uw_network_alerts.features.inc';
include_once 'uw_network_alerts_functions.inc';

/**
 * Helper function.
 *
 * Return the array of the frontpage block.
 */
function uw_network_alerts_get_frontpage_block() {

  // Get the frontpage network service alerts block, and render it.
  $block = block_load('uw_network_alerts', 'frontpage_service_alerts_block');
  $block = _block_get_renderable_array(_block_render_blocks(array($block)));
  $html = render($block);

  return array('html' => $html);
}

/**
 * Implements hook_menu().
 */
function uw_network_alerts_menu() {
  $items = [];

  // Main page: Listing of alerts.
  $items['service-uptimes'] = [
    'title' => 'Service uptimes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_network_alerts_uptime'),
    'access arguments' => ['access content'],
    'menu_name' => 'main-menu',
    'file' => 'uw_network_alerts_uptime.inc',
    'type' => MENU_NORMAL_ITEM,
  ];

  // Main page: Listing of alerts.
  $items['network-service-alerts'] = [
    'title' => 'Network and Service Alerts',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_network_alerts_page'),
    'access arguments' => ['access content'],
    'menu_name' => 'main-menu',
    'file' => 'uw_network_alerts.inc',
    'type' => MENU_NORMAL_ITEM,
  ];

  // Used for getting frontpage block.
  $items['network-service-alerts-block'] = [
    'access callback'   => true,
    'page callback'     => 'uw_network_alerts_get_frontpage_block',
    'delivery callback' => 'drupal_json_output' 
  ];

  // Service page: Listing of alerts for a given service.
  $items['network-service-alerts/%'] = [
    'title' => 'Network and Service Alerts',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_network_alerts_page', 1),
    'access arguments' => ['access content'],
    'file' => 'uw_network_alerts.inc',
    'type' => MENU_CALLBACK,
  ];
  // Individual alert details.
  $items['network-service-alert/%'] = [
    'title' => 'Network and Service Alerts',
    'page callback' => 'uw_network_alerts_alert',
    'page arguments' => [2],
    'access arguments' => ['access content'],
    'file' => 'uw_network_alerts.inc',
    'type' => MENU_CALLBACK,
  ];
  // 404 anything else.
  $items['network-service-alerts/%/%'] = [
    'page callback' => 'drupal_not_found',
    'access arguments' => ['access content'],
    'type' => MENU_CALLBACK,
  ];
  return $items;
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * Set wide page for important-dates paths.
 */
function uw_network_alerts_preprocess_html(&$variables) {

  $add_wide = FALSE;

  // Get the URL.
  $args = arg();

  if (in_array('network-service-alerts', $args)) {
    $variables['classes_array'][] = 'wide';
  }

  if (in_array('service-uptimes', $args)) {
    $variables['classes_array'][] = 'wide';
  }

  if (in_array('network-service-alert', $args)) {
    $variables['classes_array'][] = 'wide';
  }

  // Get the frontpage network service alerts block, and render it.
  $block = block_load('uw_network_alerts', 'frontpage_service_alerts_block');
  $block = _block_get_renderable_array(_block_render_blocks(array($block)));
  $html = render($block);

   // The variable to add to Drupal.settings JS.
  $varstoadd = array('network_alerts_url' => 'network-service-alerts-block');

  // Add the JS with the variables.
  drupal_add_js($varstoadd, 'setting');
}

/**
 * Load a single alert.
 *
 * @param int $alert_id
 *   The ID of the alert.
 *
 * @return object
 *   The alert object.
 */
function uw_network_alerts_alert_load($alert_id) {
  $query = "SELECT alerts.*, alertservice.description AS service_description
    FROM alerts
      LEFT JOIN alertservice ON alerts.service = alertservice.v
    WHERE ((privacy <> 'PRIVATE') OR (tweeted <> 0))
      AND sequence = :alert_id";
  $args = ['alert_id' => (int) $alert_id];
  return Database::getConnection('default', 'ona')->query($query, $args)->fetch();
}

/**
 * Implements hook_block_info().
 */
function uw_network_alerts_block_info(){
  $blocks['frontpage_service_alerts_block'] = array(
    'info' => t('Downed Services'),
    'visibility' => 1,
    'pages' => "<front>",
    'region' => 'main_content'
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function uw_network_alerts_block_view($delta = ''){
  switch ($delta){
    case 'frontpage_service_alerts_block':
      require_once 'uw_network_alerts.inc';
      $block = uw_network_alerts_frontpage_block();
      return ($block);
  }
}

/**
 * Implements hook_preprocess_item_list()
 *
 * ISTWCMS-2760.
 * Fix the pager going to page 1 issue.
 */
function uw_network_alerts_preprocess_item_list(&$vars) {

  $args = arg();

  // If we are on the network alerts page, change the pager.
  if (in_array('network-service-alerts', $args)) {

    // If we are on the pager item list, change the pager.
    if (isset($vars['attributes']['class']) && is_array($vars['attributes']['class']) &&  in_array('pager', $vars['attributes']['class'])) {

      // The array holding the current page.
      global $pager_page_array;

      // NOTE: we can use hard coded array numbers here, since the array will always be in
      // the same order (first, previous, 1).

      // If the the first page link in the pager, remove page parameter.
      if (strpos($vars['items'][0]['data'], 'page=') !== FALSE) {
        $vars['items'][0]['data'] = preg_replace('/page\=[0-9]*/', '', $vars['items'][0]['data']);
      }

      // If we are on the previous page and there is page 1 in parameter
      // and we are not currently on page 2, remove it.
      if (strpos($vars['items'][1]['data'], 'Go to previous page') !== FALSE && strpos($vars['items'][1]['data'], 'page=1') !== FALSE && $pager_page_array[0] !== 2) {
        $vars['items'][1]['data'] = str_replace('page=1', '', $vars['items'][1]['data']);
      }

      // If the first page is showing, remove the page parameter.
      if (strpos($vars['items'][2]['data'], '<a title="Go to page 1"') !== FALSE) {
        $vars['items'][2]['data'] = str_replace('page=1', '', $vars['items'][2]['data']);
      }
    }
  }
}