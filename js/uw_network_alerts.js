/**
 * @file
 */
(function ($, Drupal) {
  'use strict';
  // In order to varnish proof the network alerts, we need to load them in via js.
  // Here we will load them in on ready and the will not be behind varnish.

  Drupal.behaviors.networkAlertsVarnishProofRandom = {
    
    attach: function (context) {

      // If there is network alert and its ready, process it.
      $('.network-alerts-wrapper').ready(function () {

        // Get the data from the actual view to put in the dom.
        $.ajax({
          url: Drupal.settings.network_alerts_url,
          type: 'GET',
          success: function (response) {
            // If we have success, replace the html and show the services.
            $('.network-alerts-wrapper').replaceWith(response.html);
            $('.network-alerts-wrapper').show();
          },
          dataType: 'json'
        });
      });
    }
  };
})(jQuery, Drupal);
