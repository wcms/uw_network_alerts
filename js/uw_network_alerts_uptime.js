/**
 * @file
 */
(function ($, Drupal) {
  'use strict';
  // In order to varnish proof the network alerts, we need to load them in via js.
  // Here we will load them in on ready and the will not be behind varnish.

  Drupal.behaviors.networkAlertsUptime = {
    
    attach: function (context) {

      // If there is network alert and its ready, process it.
      $('.uw-network-alerts-uptimes').ready(function () {

        // Draw the actual timeline.
        draw_timeline();
      });

      // If the window resizes, delete all svgs and redraw timeline.
      $(window).on('resize', function() {

        // Delete all the svgs.
        $('.uw-network-alerts-timeline svg').each(function () {
          $(this).remove();
        });

        // Redraw the timeline.
        draw_timeline();
      });

      // Function to draw the timeline.
      function draw_timeline() {

        // Get the content width so that we can make the
        // timeline the same width as the content.
        var content_width = $('#content').width();

        // Set the timeline height.
        var timeline_height = 105;

        // Function use to parse date.
        var parseDate = d3.time.format("%m/%d/%Y").parse;

        // Get the data from drupal settings.
        var data = Drupal.settings.timeline_data;

        // Get the data from drupal settings.
        var timeline_info = Drupal.settings.timeline_info;

        // Get the number of days in the timeline.
        var days_for_timeline = Drupal.settings.days_for_timeline;

        // Set the margins, and width and height with margins.
        var margin = {top: 0, right: 20, bottom: 60, left: 50},
        width = content_width - margin.left - margin.right,
        height = timeline_height - margin.top - margin.bottom;

        // Set the bar width.
        var bar_width = Math.floor(width / days_for_timeline);

        // If the bar width is more than 1 subtract, so we have
        // spacing between each bar.
        if (bar_width > 1) {
          bar_width = bar_width - 1;
        }

        // Get the todate from code.
        var todate_from_code = Drupal.settings.todate;
        var todate_parts = todate_from_code.split('-');
        var todate = new Date(todate_parts[0], todate_parts[1]-1, todate_parts[2]);

        // Get the fromdate from code.
        var fromdate_from_code = Drupal.settings.fromdate;
        var fromdate_parts = fromdate_from_code.split('-');
        var fromdate = new Date(fromdate_parts[0], fromdate_parts[1]-1, fromdate_parts[2]);

        // Set the y scale of the graph.
        var y = d3.scale.ordinal()
          .rangeRoundBands([0, 5], .1);

        // Function use to parse date.
        var parseDate = d3.time.format("%m/%d/%Y").parse;

        // Set the scale of the x axis using domain and range.
        var x = d3.time.scale()
	        .domain([fromdate, todate])
          .range([0, width]);

        // Set the scale of the y axis.
        var yAxis = d3.svg.axis()
          .scale(y)
          .orient("left");

        // Setup the x-axis.
        var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom")
          .tickFormat(d3.time.format("%b %e, %Y"));

        // Tooltip div.
        var div = d3.select('.uw-network-alerts-uptimes').append('div')
          .attr('class', 'tooltip')
          .attr('aria-hidden', 'true')
          .style('display', 'none');

        // Arrays to be used to store the different types of alerts.
        var ups = [];
        var incidents = [];
        var maints = [];

        for (var key in data) {

          // Need to set the arrays to NULL so that get fresh data for every service.
          ups = [];
          incidents = [];
          maints= [];

          // Step through each data and get the ups, incidents and mainteances.
          data[key].forEach(function(d, i) {

            // We will always have ups, so simply push them to the array.
            ups.push(data[key][i].up);

            // Check if there are incidents and if so push to the array.
            if ('Incident' in data[key][i]) {
              incidents.push(data[key][i].Incident);
            }

            // Check if there are mainteances and if so push to the array.
            if ('Maintenance' in data[key][i]) {
              maints.push(data[key][i].Maintenance);
            }
          });

          // Step through each of the ups and set the time format.
          ups.forEach(function(up) {
            up.time = parseDate(up.time_date);
          });

          // If there are incidents, step through each and set the time format.
          if (incidents.length > 0) {
            incidents.forEach(function(incident) {
              incident.time = parseDate(incident.time_date);
            });
          }

          // If there are mainteances, step through each and set the time format.
          if (maints.length > 0) {
            maints.forEach(function(maint) {
              maint.time = parseDate(maint.time_date);
            });
          }

          // Set the svg on the page.
          // The transform turns the dates 45 degrees.
          var svg = d3.select('.' + timeline_info[key].class_name).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

          // The x-axis svg.
          svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

          // The bars where there are network alerts.
          svg.selectAll(".bar")
            .data(ups)
            .enter().append("rect")
            .attr("class", function(u) { return "bar " + u.type; })
            .attr("y", function(u) { return y(u.type); })
            .attr("height", y.rangeBand())
            .attr("x", function(u) { return x(u.time); })
            .attr("width", bar_width)
            .on('mouseover', mouseover)
            .on('mousemove', mousemove)
            .on('mouseout', mouseout);

          // The bars where there are network alerts.
          svg.selectAll(".bar2")
            .data(incidents)
            .enter().append("rect")
            .attr("class", function(i) { return "bar " + i.type; })
            .attr("y", function(i) { return y(i.type); })
            .attr("height", function(i) { return i.height; })
            .attr("x", function(i) { return x(i.time); })
            .attr("width", bar_width)
            .on('mouseover', mouseover)
            .on('mousemove', mousemove)
            .on('mouseout', mouseout);

          // The bars where there are network alerts.
          svg.selectAll(".bar3")
            .data(maints)
            .enter().append("rect")
            .attr("class", function(m) { return "bar " + m.type; })
            .attr("y", function(m) { return y(m.type); })
            .attr("height", function(m) { return m.height; })
            .attr("x", function(m) { return x(m.time); })
            .attr("width", bar_width)
            .on('mouseover', mouseover)
            .on('mousemove', mousemove)
            .on('mouseout', mouseout);

          // Set the date rotation on the x-axis.
          svg.selectAll(".tick text")  // select all the text elements for the xaxis
            .attr("transform", function(d) {
                return "translate(" + this.getBBox().height*-2 + "," + this.getBBox().height*2 + ")rotate(-45)";
          });

          // Function for tooltip mouseover.
          function mouseover(d, i) {
            div.style('display', 'inline');
          }

          // Function for tooltip mousemove.
          function mousemove(d, i){
              if (d.type != 'Up' && d.interval != '') {
                div
                    .html(d.type + '<br />' + d.time_date_display + '<br />Duration: ' + d.interval)
                    .style('left', (d3.event.pageX - 34) + 'px')
                    .style('top', (d3.event.pageY - 12) + 'px');
              }
              else if (d.type != 'Up') {
                div
                    .html(d.type + '<br />' + d.time_date_display)
                    .style('left', (d3.event.pageX - 34) + 'px')
                    .style('top', (d3.event.pageY - 12) + 'px');
              }
              else {
                div
                    .html('No downtime' + '<br />' + d.time_date_display)
                    .style('left', (d3.event.pageX - 34) + 'px')
                    .style('top', (d3.event.pageY - 12) + 'px');
              }
          }

          // Function for tooltip mouseout.
          function mouseout(d, i){
            div.style('display', 'none');
          }
        }
      }
    }
  };
})(jQuery, Drupal);
