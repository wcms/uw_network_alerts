/**
 * @file
 */
(function ($, Drupal) {
  'use strict';
  // In order to varnish proof the network alerts, we need to load them in via js.
  // Here we will load them in on ready and the will not be behind varnish.

  Drupal.behaviors.networkAlertsTimeline = {
    
    attach: function (context) {

      // If there is network alert and its ready, process it.
      $('.uw-network-alerts-timeline').ready(function () {

        // Draw the actual timeline.
        draw_timeline();
      });

      // If the window resizes, delete all svgs and redraw timeline.
      $(window).on('resize', function() {

        // Delete all the svgs.
        $('.uw-network-alerts-timeline svg').each(function () {
          $(this).remove();
        });

        // Redraw the timeline.
        draw_timeline();
      });

      // Function to draw the timeline.
      function draw_timeline() {

        // Get the content width so that we can make the
        // timeline the same width as the content.
        var content_width = $('#content').width();

        // Set the timeline height.
        var timeline_height = 105;

        // Function use to parse date.
        var parseDate = d3.time.format("%m/%d/%Y").parse;

        // Get the data from drupal settings.
        var data = Drupal.settings.timeline_data;

        var ups = Drupal.settings.ups;
        var incidents = Drupal.settings.incidents;
        var maintenances = Drupal.settings.maintenances;

        ups.forEach(function(up) {
          up.time = parseDate(up.time_date);
        });

        if (typeof(incidents) !== 'undefined') {
          incidents.forEach(function(incident) {
            incident.time = parseDate(incident.time_date);
          });
        }

        if (typeof(maintenances) !== 'undefined') {
          maintenances.forEach(function(maintenance) {
            maintenance.time = parseDate(maintenance.time_date);
          });
        }

        // Get the number of days in the timeline.
        var days_for_timeline = Drupal.settings.days_for_timeline;

        // Set the margins, and width and height with margins.
        var margin = {top: 0, right: 20, bottom: 60, left: 50},
        width = content_width - margin.left - margin.right,
        height = timeline_height - margin.top - margin.bottom;

        // Set the bar width.
        var bar_width = Math.floor(width / days_for_timeline);

        // If the bar width is more than 1 subtract, so we have
        // spacing between each bar.
        if (bar_width > 1) {
          bar_width = bar_width - 1;
        }

        // Get the todate from code.
        var todate_from_code = Drupal.settings.todate;
        var todate_parts = todate_from_code.split('-');
        var todate = new Date(todate_parts[0], todate_parts[1]-1, todate_parts[2]);

        // Get the fromdate from code.
        var fromdate_from_code = Drupal.settings.fromdate;
        var fromdate_parts = fromdate_from_code.split('-');
        var fromdate = new Date(fromdate_parts[0], fromdate_parts[1]-1, fromdate_parts[2]);

        // Set the y scale of the graph.
        var y = d3.scale.ordinal()
          .rangeRoundBands([0, 5], .1);

        // Set the scale of the x axis using domain and range.
        var x = d3.time.scale()
	        .domain([fromdate, todate])
          .range([0, width]);

        // Set the scale of the y axis.
        var yAxis = d3.svg.axis()
          .scale(y)
          .orient("left");

        // Setup the x-axis.
        var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom")
          .tickFormat(d3.time.format("%b %e, %Y"));

        // Set the svg on the page.
        // The transform turns the dates 45 degrees.
        var svg = d3.select(".uw-network-alerts-timeline").append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        // Tooltip div.
        var div = d3.select('.uw-network-alerts-timeline').append('div')
          .attr('class', 'tooltip')
          .style('display', 'none');

        // The x-axis svg.
        svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis);

        // The bars where there are network alerts.
        svg.selectAll(".bar")
          .data(ups)
          .enter().append("rect")
          .attr("class", function(d) { return "bar " + d.type; })
          .attr("y", function(d) { return y(d.type); })
          .attr("height", y.rangeBand())
          .attr("x", function(d) { return x(d.time); })
          .attr("width", bar_width)
          .on('mouseover', mouseover)
          .on('mousemove', mousemove)
          .on('mouseout', mouseout);

        if (typeof(incidents) !== 'undefined') {
          // The bars where there are network alerts.
          svg.selectAll(".bar2")
            .data(maintenances)
            .enter().append("rect")
            .attr("class", function(m) { return "bar " + m.type; })
            .attr("y", function(m) { return y(m.type); })
            .attr("height", function(m) { return m.height; })
            .attr("x", function(m) { return x(m.time); })
            .attr("width", bar_width)
            .on('mouseover', mouseover)
            .on('mousemove', mousemove)
            .on('mouseout', mouseout);
        }

        if (typeof(maintenances) !== 'undefined') {
          // The bars where there are network alerts.
          svg.selectAll(".bar3")
            .data(maintenances)
            .enter().append("rect")
            .attr("class", function(i) { return "bar " + i.type; })
            .attr("y", function(i) { return y(i.type); })
            .attr("height", function(i) { return i.height; })
            .attr("x", function(i) { return x(i.time); })
            .attr("width", bar_width)
            .on('mouseover', mouseover)
            .on('mousemove', mousemove)
            .on('mouseout', mouseout);
        }

        // Set the date rotation on the x-axis.
        svg.selectAll(".tick text")  // select all the text elements for the xaxis
          .attr("transform", function(d) {
              return "translate(" + this.getBBox().height*-2 + "," + this.getBBox().height*2 + ")rotate(-45)";
        });

        // Function for tooltip mouseover.
        function mouseover(d, i) {
          div.style('display', 'inline');
        }

        // Function for tooltip mousemove.
        function mousemove(d, i){
            if (d.type != 'Up' && d.interval != '') {
              div
                  .html(d.type + '<br />' + d.time_date_display + '<br />Duration: ' + d.interval)
                  .style('left', (d3.event.pageX - 34) + 'px')
                  .style('top', (d3.event.pageY - 12) + 'px');
            }
            else if (d.type != 'Up') {
              div
                  .html(d.type + '<br />' + d.time_date_display)
                  .style('left', (d3.event.pageX - 34) + 'px')
                  .style('top', (d3.event.pageY - 12) + 'px');
            }
            else {
              div
                  .html('No downtime' + '<br />' + d.time_date_display)
                  .style('left', (d3.event.pageX - 34) + 'px')
                  .style('top', (d3.event.pageY - 12) + 'px');
            }
        }

        // Function for tooltip mouseout.
        function mouseout(d, i){
          div.style('display', 'none');
        }
      }
    }
  };
})(jQuery, Drupal);