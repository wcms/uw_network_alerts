<?php

include_once('uw_network_alerts_classes.php');

/**
 * @file
 * Functions for Network and Service Alerts.
 */

 /**
 * Return alert object.
 *
 * @param string $sequence
 *   The alert id, called sequence.
 *
 * @return object[]
 *   An array of alert objects.
 */
function uw_network_alerts_get_alert($sequence) {

  // If there is no sequence, or it is NULL or not numeric, simply return.
  if (isset($seqeunce) && ($sequence == NULL || !is_numeric($sequence))) {
    return;
  }

  // The query to get the alert.
  $query = "
    SELECT * FROM alerts
    WHERE ((privacy <> 'PRIVATE') OR (tweeted <> 0)) AND (linkedfrom = 0) AND sequence = " . $sequence;

  $alerts = Database::getConnection('default', 'ona')->query($query);

  // Step through the alerts list and get the alert.
  foreach ($alerts as $alert_row) {
    $alert = $alert_row;
    break;
  }

  // If there is no alert, simply reach.
  // If there is an alert, return it.
  if (!isset($alert)) {
    return;
  }
  else {
    return $alert;
  }
}
 
/**
 * Return alert objects, optionally for only a particular service.
 *
 * @param string $service
 *   The name of the servce. Use NULL for all.
 * @param int $limit
 *   Return no more than this number of results. Use NULL for no limit.
 * @param int $order
 *   The field to sort by. Use NULL for default.
 * @param int $dayrange
 *   Display alerts from plus or minus this number of days. Use NULL to not
 *   filter based on time.
 *
 * @return object[]
 *   An array of alert objects.
 */
function uw_network_alerts_alert_list($service = NULL, $type = NULL, $fromdate = NULL, $todate= NULL, $limit = NULL, $order = NULL, $sort = NULL, $pager = NULL, $count = FALSE) {

  if ($count) {
    $query = "SELECT COUNT(*) as type_count ";
  }
  else {
    $query = "SELECT * ";
  }

  $query .= "FROM alerts
    WHERE ((privacy <> 'PRIVATE') OR (tweeted <> 0))
      AND (linkedfrom = 0)";
  $args = [];

  // Only a particular service.
  if ($service) {
    $query .= " AND (service REGEXP :service)";
    $args['service'] = '[[:<:]]' . $service . '[[:>:]]';
  }

  if ($type) {
    $query .= " AND (alerttype = :type)";
    $args['type'] = $type;
  }

  // If we have both and from and to date, add to query.
  if (isset($fromdate) && (isset($todate))) {

    // Add to query.
    $query .= ' AND (starttime >= :from_date) AND (starttime <= :to_date)';

    // Add the dates to the arguments for the query.
    $args['from_date'] = $fromdate;
    $args['to_date'] = $todate;
  }
  else if (isset($fromdate)) {

    // Add to query.
    $query .= ' AND (starttime >= :from_date)';

    // Add the dates to the arguments for the query.
    $args['from_date'] = $fromdate;
  }
  else if(isset($todate)) {

    // Add to query.
    $query .= ' AND (starttime <= :to_date)';

    // Add the dates to the arguments for the query.
    $args['to_date'] = $todate;
  }

  // Specify order.
  if (!$order || !preg_match('/^[a-z]+$/', $order)) {
    $order = 'sequence';
  }

  // Specify sort if none given.
  if (!$sort) {
    $sort = 'DESC';
  }

  // Add order and sort by to the query.
  $query .= " ORDER BY " . $order . " " . $sort;

  // If there as a pager add the offset to the query.
  if (isset($pager)) {
    $query .= ' LIMIT ' . ($pager * $limit) . ',' . $limit;
  }
  else if($limit !== NULL) {
    $query .= ' LIMIT ' . $limit;
  }

  return Database::getConnection('default', 'ona')->query($query, $args);
}

/**
 * Return the title of the alert.
 *
 * @param object $alert
 *   The alert object.
 * @param bool $with_time
 *   When TRUE, use the format that begins with the time.
 *
 * @return array
 *   The information about the alert.
 */
function uw_network_alerts_get_alert_title(stdClass $alert, $with_time = FALSE) {
  if ($with_time) {
    $alert_array['start_time'] = $alert->starttime;
    $alert_array['service'] = $alert->service;
    $alert_array['type'] = strtolower($alert->alerttype);
    $alert_array['description'] = $alert->description;
    return $alert_array;
  }
  elseif ((string) $alert->subject) {
    return $alert->subject;
  }
  else {
    return $alert->alerttype . ' ALERT - ' . substr($alert->starttime, 0, 10) . ' - ' . $alert->description;
  }
}


/**
 * Return service objects.
 *
 * @return object[]
 *   An array of service objects.
 */
function _uw_network_alerts_get_services() {
  $query = "SELECT DISTINCT v
    FROM alertservice
    ORDER BY v";

  return Database::getConnection('default', 'ona')->query($query);
}

/**
 * Return alert type objects.
 *
 * @return object[]
 *   An array of service objects.
 */
function _uw_network_alerts_get_types() {
  $query = "SELECT DISTINCT area
    FROM alertservice
    ORDER BY area";

  return Database::getConnection('default', 'ona')->query($query);
}

/**
 * Helper function.
 *
 * This function to submit settings form for important dates.
 */
function _uw_network_alerts_filter_submit($form, &$form_state) {

  $query_parameters = drupal_get_query_parameters();

  // If the reset button is not clicked, get the parameters.
  // If the rest button is clicked, set order and sort parameters to defaults.
  if ($form_state['clicked_button']['#value'] !== 'Reset') {

    if (isset($form_state['values']['end'])) {
      $parameters['to_date_filter'] = $form_state['values']['end'];
    }
  
    if (isset($form_state['values']['start'])) {
      $parameters['from_date_filter'] = $form_state['values']['start'];
    }
  
    if (isset($form_state['values']['service'])) {
      $parameters['service_filter'] = $form_state['values']['service'];
    }
  
    if (isset($form_state['values']['type'])) {
      $parameters['type_filter'] = $form_state['values']['type'];
    }

    // Check if there are sort and order parameters and if so add them.
    if (isset($query_parameters) && isset($query_parameters['order'])) {
      $parameters['order'] = $query_parameters['order'];
      $parameters['sort'] = $query_parameters['sort'];
    }
  }
  else {

    // Default order and sort parameters.
    $parameters['order'] = 'starttime';
    $parameters['sort'] = 'desc';
  }

  // If there are parameters go to the link with the parameters.
  // If there are no parameters then go to the link without paramters.
  if (isset($parameters)) {

    // Go to the network alerts page with the parameters.
    drupal_goto('/network-service-alerts', array('query' => $parameters));
  }
  else {

    // Go to the network alerts page without any paramters (i.e. Reset button clicked).
    drupal_goto('/network-service-alerts');
  }
}

/**
 * Return the name of a type ready for user display.
 *
 * It will use the name from an override list if there is one for the 'area'
 * property. Otherwise, if present, it will use the 'area' property.
 *
 * @param string $type
 *   The type
 *
 * @return string
 *   The long type name having been run through check_plain().
 */
function uw_network_alerts_get_type_name($type = NULL) {
  if ($type == NULL) {
    return;
  }

  static $name_override = [
    'ClientServices' => 'Client Services',
    'EA' => 'Enterprise Architecture',
    'ES-Applications' => 'Enterprise Systems-Applications',
    'NETWORK' => 'Network',
    'SERVICE' => 'Service'
  ];

  if (isset($type)) {
    if (isset($name_override[$type])) {
      return $name_override[$type];
    }
    else {
      return $type;
    }
  }
}

/**
 * Return the name of a service ready for user display.
 *
 * It will use the name from an override list if there is one for the 'v'
 * property. Otherwise, if present, it will use the 'description' property.
 * Otherwise, it will query the name from the database using the 'v' property.
 * If that does not return a value, it will use the 'v' property as the name.
 *
 * @param object $service
 *   The service object which must contain at minimum a 'v' or non-empty
 *   'description' property.
 *
 * @return string
 *   The service name having been run through check_plain().
 */
function uw_network_alerts_get_service_name(stdClass $service = NULL, $alert_services = NULL) {

  if ($service == NULL && $alert_services == NULL) {
    return;
  }

  static $name_override = [
    'BES' => 'BlackBerry Enterprise Server (BES)',
    'CAS' => 'Central Authentication Service (CAS)',
    'CRAC' => 'Computer room air conditioning',
    'EXCHANGE' => 'Exchange (Connect)',
    'MSSQL' => 'Hosted databases: Microsoft SQL Server',
    'LEARN' => 'Learn',
    'MAILSERVICES' => 'Mail services',
    'Office 365' => 'Microsoft Office 365 Education',
    'SCCM' => 'Microsoft SCCM',
    'SKYPE' => 'Skype for Business',
    'VPN' => 'Virtual Private Network (VPN)',
    'WCMS' => 'Waterloo Content Management System (WCMS)',
  ];

  if (isset($service)) {
    if (isset($service->v) && isset($name_override[$service->v])) {
      return $name_override[$service->v];
    }
    elseif (!isset($service->description)) {
      $query = "SELECT description FROM alertservice WHERE v = :v";
      $args = ['v' => $service->v];
      $service->description = Database::getConnection('default', 'ona')->query($query, $args)->fetchField();
    }
    return check_plain($service->description ?: $service->v);
  }
  else if (isset($alert_services)) {
    $services = explode(',', $alert_services);
  
    $name_count = 0;
    $service_names = '';

    foreach ($services as $service) {
      if (isset($name_override[$service])) {
        if ($name_count > 0) {
          $service_names .= ', ';
        }
        $service_names .= $name_override[$service];
        $name_count++;
      }
      else {
        if ($name_count > 0) {
          $service_names .= ', ';
        }
        $service_names .= ucwords(strtolower($service));
        $name_count++;
      }
    }

    return $service_names;
  }
}

/**
 * Return the uptime average for a service.
 *
 * @param string $service
 *   The name of the servce.
 * @param int $start
 *   The timestamp of the start of the period.
 * @param int $end
 *   The timestamp of the end of the period.
 * @param string $maint
 *   When calculating, limit to alerts with this value in the 'maint' column.
 * @param string $nonprod
 *   When calculating, limit to alerts with this value in the 'nonprod' column.
 *
 * @return float
 *   The uptime average.
 */
function uw_network_alerts_get_service_uptime($service, $start, $end, $maint = NULL, $nonprod = NULL) {
  $query = "SELECT 100 - SUM(degree * scope * (unix_timestamp(endtime) - unix_timestamp(starttime))) / ((:end - :start) * 100) AS uptime
    FROM alerts
    WHERE (incidenttype = 'INCIDENT')
      AND (service REGEXP :service)
      AND (linkedfrom = 0)
      AND (endtime <> '0000-00-00 00:00:00')
      AND (unix_timestamp(endtime) > :start)
      AND (unix_timestamp(endtime) <= :end)";
  $args = [
    'service' => '[[:<:]]' . $service . '[[:>:]]',
    'start' => $start,
    'end' => $end,
  ];
  if ($maint) {
    $query .= " AND (maint = :maint)";
    $args['maint'] = $maint;
  }
  if ($nonprod) {
    $query .= " AND (nonprod = :nonprod)";
    $args['nonprod'] = $nonprod;
  }
  $uptime = Database::getConnection('default', 'ona')->query($query, $args)->fetchField();
  // If there are no alerts, return 100%. Otherwise, round and return no better
  // than 99.999%.
  if ($uptime) {
    $uptime = round($uptime, 10);
    if ($uptime != 100) {
      $uptime = round($uptime, 3);
      if ($uptime == 100) {
        return 99.999;
      }
    }
    return $uptime;
  }
  else {
    return 100;
  }
}

/**
 * Return the status of a service.
 *
 * @param string $service
 *   The name of the service.
 *
 * @return string
 *   The status.
 */
function uw_network_alerts_get_service_status($service) {
  $status = 'up';

  $query = "SELECT 1,
      alertdegree.colour AS alertdegree_colour,
      alertscope.colour AS alertscope_colour
    FROM alerts
      LEFT JOIN alertdegree ON alerts.degree = alertdegree.v
      LEFT JOIN alertscope ON alerts.scope = alertscope.v
    WHERE (incidenttype = 'INCIDENT')
      AND (privacy <> 'PRIVATE')
      AND (service REGEXP :service)
      AND (linkedfrom = 0)
      AND (endtime = '0000-00-00 00:00:00')";
  $args = ['service' => '[[:<:]]' . $service . '[[:>:]]'];
  $result = Database::getConnection('default', 'ona')->query($query, $args);
  while ($alert = $result->fetch()) {
    $status = 'degraded';
    if ($alert->alertdegree_colour === 'red' && $alert->alertscope_colour === 'red') {
      return 'down';
    }
  }

  $query = "SELECT degree, scope
    FROM alerts
    WHERE ((incidenttype = 'MAINTENANCE') OR (incidenttype = 'EMERGENCY-MAINTENANCE'))
      AND (privacy <> 'PRIVATE')
      AND (service REGEXP :service)
      AND (linkedfrom = 0)
      AND (unix_timestamp(starttime) < unix_timestamp(now()))
      AND (unix_timestamp(endtime) > unix_timestamp(now()))";
  $args = ['service' => '[[:<:]]' . $service . '[[:>:]]'];
  $result = Database::getConnection('default', 'ona')->query($query, $args);
  while ($alert = $result->fetch()) {
    $status = 'nonsvcmaint';
    if (($alert->degree > 0) && ($alert->scope > 0)) {
      return 'maint';
    }
  }

  if ($status === 'up') {
    $query = "SELECT 1
      FROM alerts
      WHERE ((incidenttype = 'MAINTENANCE') OR (incidenttype = 'EMERGENCY-MAINTENANCE'))
        AND (privacy <> 'PRIVATE')
        AND (service REGEXP :service)
        AND (linkedfrom = 0)
        AND (unix_timestamp(starttime) > unix_timestamp(now()))";
    $args = ['service' => '[[:<:]]' . $service . '[[:>:]]'];
    $result = Database::getConnection('default', 'ona')->query($query, $args);
    if ($result->rowCount()) {
      return 'futuremaint';
    }
  }

  return $status;
}

/**
 * Helper function
 *
 * Get the timeline data for the uptime graph.
 */
function _uw_network_alerts_get_timeline($timeline, $service, $fromdate, $todate, $fromdate_test, &$total_seconds) {

  // Step through each alert and get the time and type.
  foreach (uw_network_alerts_alert_list($service, '', $fromdate, $todate) as $alert_row) {

    // Ensure that there is a proper start and end date, before processing the row.
    if (strtotime($alert_row->starttime) > 0 && strtotime($alert_row->endtime) > 0) {

      // Date to test to ensure that we are between 90 days.
      $time_date = date("m/j/Y", strtotime($alert_row->starttime));

      // If the the date is greater than 90 days ago, process it.
      if (strtotime($time_date) >= strtotime($fromdate_test)) {

        // Get the timestamp in the correct format.
        $insert_date = date("m/j/Y", strtotime($alert_row->starttime));

        // Check if the date is the array and if so get the key and replace it.
        if($key = array_search($insert_date, array_column($timeline, 'time_date'))) {

          // DateTimeInterfaces for start and end times of alert.
          $start_time = new DateTime($alert_row->starttime);
          $end_time = new DateTime($alert_row->endtime);

          // The time of the alert.
          //$timeline[$key]['time_date'] = date("m/j/Y", strtotime($alert_row->starttime));
          $entry['time_date'] = date("m/j/Y", strtotime($alert_row->starttime));

          // The difference between the start and end times.
          $interval = date_diff($start_time, $end_time);

          // Format the interval for display and get the number of seconds
          // of downtime that will be used in the uptime calculation.
          if ($interval->d >= 1) {
            $interval_display = $interval->d . 'd ' . $interval->h . 'h ' . $interval->i . 'm';
            $seconds = (($interval->d * 24 + $interval->h) * 60 + $interval->i) * 60 + $interval->s;
          }
          else if ($interval->h >= 1) {
            $interval_display = $interval->h . 'h ' . $interval->i . 'm';
            $seconds = (($interval->h) * 60 + $interval->i) * 60 + $interval->s;
          }
          else if ($interval->i >= 1) {
            $interval_display = $interval->i . 'm';
            $seconds = $interval->i * 60 + $interval->s;
          }
          else if ($interval->s >= 1) {
            $interval_display = $interval->s . 's';
            $seconds = $interval->s;
          }

          // Track the total seconds of downtime to be used in the uptime calculation.
          if (isset($seconds) && $alert_row->incidenttype == "INCIDENT") {
            $total_seconds += $seconds;
          }

          // Set the variables, based on if there is an interval.
          if (isset($interval_display)) {
            $entry['time_date_display'] = date("M j, Y", strtotime($alert_row->starttime));
            $entry['interval'] = $interval_display;
            $entry['interval_raw'] = $interval;
          }
          else {
            $entry['time_date_display'] = date("M j, Y g:i", strtotime($alert_row->starttime));
          }

          // Set the type.
          $entry['type'] = ucfirst(strtolower($alert_row->incidenttype));

          $timeline[$key]['alerts'][] = $entry;
        }
      }
    }
  }

  // Step through the timeline and sort into Up, Incident and Mainteance.
  foreach ($timeline as $key => $tl) {

    // Setup the uptime entry.
    $new_entry = array(
      'time_date' => $timeline[$key]['time_date'],
      'time_date_display' => $timeline[$key]['time_date_display'],
      'type' => $timeline[$key]['type'],
    );

    // Put in the uptime entry.
    $new_timeline[$key]['up'] = $new_entry;

    // If there are extra alerts (Incident or Mainteance), add to array.
    if (isset($tl['alerts'])) {
      foreach ($tl['alerts'] as $alert) {
        $new_timeline[$key][$alert['type']][] = $alert;
      }
    }
  }

  // Step through each of the new timeline and setup height.
  foreach ($new_timeline as $key => $ntl) {

    foreach($ntl as $new_key => $nt) {

      // If the key is up, just add the info, no need for height calculation, will always be 100%.
      if ($new_key == "up") {

        $latest_timeline[$key][$new_key] = $nt;
      }
      else {

        // If there is only one entry, calculate height.
        if (count($nt) == 1) {

          // Set the entry.
          $latest_timeline[$key][$new_key] = $nt[0];

          // Calculate the seconds using the interval object.
          if ($latest_timeline[$key][$new_key]['interval_raw']->d >= 1) {
            $seconds = (($latest_timeline[$key][$new_key]['interval_raw']->d * 24 + $latest_timeline[$key][$new_key]['interval_raw']->h) * 60 + $latest_timeline[$key][$new_key]['interval_raw']->i) * 60 + $latest_timeline[$key][$new_key]['interval_raw']->s;
          }
          else if ($latest_timeline[$key][$new_key]['interval_raw']->h >= 1) {
            $seconds = (($latest_timeline[$key][$new_key]['interval_raw']->h) * 60 + $latest_timeline[$key][$new_key]['interval_raw']->i) * 60 + $latest_timeline[$key][$new_key]['interval_raw']->s;
          }
          else if ($latest_timeline[$key][$new_key]['interval_raw']->i >= 1) {
            $seconds = $latest_timeline[$key][$new_key]['interval_raw']->i * 60 + $latest_timeline[$key][$new_key]['interval_raw']->s;
          }
          else if ($latest_timeline[$key][$new_key]['interval_raw']->s >= 1) {
            $seconds = $latest_timeline[$key][$new_key]['interval_raw']->s;
          }

          // Set the height based on 24 hours during the day and the height of 45.
          $latest_timeline[$key][$new_key]['height'] = $seconds > 86400 ? 45 : $seconds / 86400 * 45;
        }
        else {

          // We need to loop through the alerts and calcualte the total interval.
          // Count variable for setting the interval object.
          $count = 0;

          // Step through each entry and add up the intervals.
          foreach ($nt as $n) {

            // If the count is 0, setup the intial interval object.
            if ($count == 0) {

              // Initial interval object.
              $e = MyDateInterval::fromDateInterval($n['interval_raw']);
            }
            else {

              // Already have initial interval object, so simply add the remaining intervals.
              $e->add($n['interval_raw']);
            }

            // Increment the counter so we dont reset the intital interval object.
            $count++;
          }

          // Format the interval for display and get the number of seconds
          // of downtime that will be used in the uptime calculation.
          if ($e->d >= 1) {
            $interval_display = $e->d . 'd ' . $e->h . 'h ' . $e->i . 'm';
            $seconds = (($e->d * 24 + $e->h) * 60 + $e->i) * 60 + $e->s;
          }
          else if ($e->h >= 1) {
            $interval_display = $e->h . 'h ' . $e->i . 'm';
            $seconds = (($e->h) * 60 + $e->i) * 60 + $e->s;
          }
          else if ($e->i >= 1) {
            $interval_display = $e->i . 'm';
            $seconds = $e->i * 60 + $e->s;
          }
          else if ($e->s >= 1) {
            $interval_display = $e->s . 's';
            $seconds = $e->s;
          }

          // Set the entry.
          $latest_timeline[$key][$new_key] = array(
            'time_date' => $nt[0]['time_date'],
            'time_date_display' => $nt[0]['time_date_display'],
            'type' => $new_key,
            'interval' => $interval_display,
            'height' => $seconds > 86400 ? 45 : $seconds / 86400 * 45,
          );

        }
      }
    }
  }

  return $latest_timeline;
}
